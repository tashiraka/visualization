zoom = 80

def setup():
    size(650, 600, P3D)
        
        
def draw():
    background(200) #black background
    translate(width/2, height/2) #set the origin on the center of display
    camera(zoom, 0, 0, 0, 0, 0, 0, 1, 0)
    rotateX(radians(100))
    rotateY(radians(100))
    rotateZ(radians(270))
    rotateY(radians(mouseY))
    rotateZ(radians(mouseX))
    
    path = '/home/yuichi/Workspace/Hi-C_workbench/visualization/visualize_simplicial_complex/'
    filtration_file = path + 'filtration'
    nm = 35
    filtered_deg = nm * 0.062
    out_file = path + 'filtration_' + str(nm) + '.jpg'
    
    for line in open(filtration_file, 'r'):
        fields = line.rstrip().split('\t')
        dim = len(fields) - 2
        deg = float(fields[0])
                
        if deg < filtered_deg:    
            points = []
             
            for field in fields[1:]:
                points.append(map(float, field.split(' ')))
                                
            if dim == 0:
                fill(255, 150)
                beginShape(POINTS)
                vertex(points[0][0], points[0][1], points[0][2])
                endShape()

            elif dim == 1:
                fill(255, 150)
                beginShape(LINES)
                vertex(points[0][0], points[0][1], points[0][2])
                vertex(points[1][0], points[1][1], points[1][2])
                endShape()

            elif dim == 2:
                fill(255, 150)
                beginShape(TRIANGLE)
                vertex(points[0][0], points[0][1], points[0][2])
                vertex(points[1][0], points[1][1], points[1][2])
                vertex(points[2][0], points[2][1], points[2][2])
                endShape()

            elif dim == 3:
                fill(255, 150)
                beginShape(TRIANGLE_STRIP)
                vertex(points[0][0], points[0][1], points[0][2])
                vertex(points[1][0], points[1][1], points[1][2])
                vertex(points[2][0], points[2][1], points[2][2])
                vertex(points[3][0], points[3][1], points[3][2])
                vertex(points[1][0], points[1][1], points[1][2])
                vertex(points[2][0], points[2][1], points[2][2])
                endShape()

            else:
                print "ERROR: dim > 3"
                quit()        
    
    saveFrame(out_file)

def mouseWheel(event):
    global zoom 
    zoom += event.getCount() * 20

