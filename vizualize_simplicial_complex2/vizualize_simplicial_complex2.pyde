zoom = 1300

def setup():
    size(650, 600, P3D)
        
        
def draw():
    background(200) #black background
    translate(width/2, height/2) #set the origin on the center of display
    camera(zoom, 0, 0, 0, 0, 0, 1, 1, 0)
    rotateY(radians(mouseX))
    rotateZ(radians(mouseY))
    
    file = '/home/yuichi/Workspace/Hi-C_workbench/filtration/vizualize_simplicial_complex2/test.xyz'
    points = []            
    for line in open(file, 'r'):
        points.append(map(float, line.rstrip().split('\t')))
    
    #fill(255, 150)
    #beginShape(TRIANGLE_STRIP)
    #vertex(points[0][0], points[0][1], points[0][2])
    #vertex(points[1][0], points[1][1], points[1][2])
    #vertex(points[2][0], points[2][1], points[2][2])
    #vertex(points[3][0], points[3][1], points[3][2])
    #vertex(points[1][0], points[1][1], points[1][2])
    #vertex(points[2][0], points[2][1], points[2][2])
    
    beginShape(TRIANGLE)
    vertex(points[0][0], points[0][1], points[0][2])
    vertex(points[1][0], points[1][1], points[1][2])
    vertex(points[2][0], points[2][1], points[2][2])
    endShape()
    beginShape(TRIANGLE)
    vertex(points[0][0], points[0][1], points[0][2])
    vertex(points[1][0], points[1][1], points[1][2])
    vertex(points[3][0], points[3][1], points[3][2])
    endShape()
    beginShape(TRIANGLE)
    vertex(points[0][0], points[0][1], points[0][2])
    vertex(points[2][0], points[2][1], points[2][2])
    vertex(points[3][0], points[3][1], points[3][2])
    endShape()
    #beginShape(TRIANGLE)
    #vertex(points[1][0], points[1][1], points[1][2])
    #vertex(points[2][0], points[2][1], points[2][2])
    #vertex(points[3][0], points[3][1], points[3][2])
    #endShape()

def mouseWheel(event):
    global zoom 
    zoom += event.getCount() * 20

