path = "/home/yuichi/Workspace/Hi-C_workbench/visualization/visualize_ball_filtration/"
pdb_file = path + "3Dcoord.chrII.pdb"
save_file = path + "test.png"

cmd.delete("all")
cmd.load(pdb_file)

cmd.bg_color("white")

cmd.show("spheres")
cmd.spectrum()
cmd.set("sphere_mode", 5)
cmd.set("sphere_transparency", 0)
cmd.util.cbaw
cmd.set("light_count",8)
cmd.set("spec_count",1)
cmd.set("shininess", 10)
cmd.set("specular", 0.25)
cmd.set("ambient",0)
cmd.set("direct",0)
cmd.set("reflect",1.5)
cmd.set("ray_shadow_decay_factor", 0.1)
cmd.set("ray_shadow_decay_range", 2)
cmd.unset("depth_cue")

cmd.set("sphere_scale", 800)
cmd.ray(600, 600)
cmd.png(save_file, dpi=100)
