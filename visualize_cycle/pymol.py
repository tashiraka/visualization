path = "/home/yuichi/Workspace/Hi-C_workbench/visualization/visualize_ball_filtration/"
pdb_file = path + "3Dcoord.pdb"
save_file_0 = path + "3Dcoord.0.png"
save_file_10 = path + "3Dcoord.10.png"
save_file_30 = path + "3Dcoord.30.png"
save_file_60 = path + "3Dcoord.60.png"
save_file_100 = path + "3Dcoord.100.png"
save_file_500 = path + "3Dcoord.500.png"

cmd.delete("all")
cmd.load(pdb_file)

cmd.bg_color("white")

cmd.show("spheres")
cmd.spectrum()
cmd.set("sphere_mode", 5)
cmd.set("sphere_transparency", 0)
cmd.util.cbaw
cmd.set("light_count",8)
cmd.set("spec_count",1)
cmd.set("shininess", 10)
cmd.set("specular", 0.25)
cmd.set("ambient",0)
cmd.set("direct",0)
cmd.set("reflect",1.5)
cmd.set("ray_shadow_decay_factor", 0.1)
cmd.set("ray_shadow_decay_range", 2)
cmd.unset("depth_cue")

cmd.set("sphere_scale", 0)
cmd.ray(2400, 2400)
cmd.png(save_file_0, dpi=300)

cmd.set("sphere_scale", 10)
cmd.ray(2400, 2400)
cmd.png(save_file_10, dpi=300)

cmd.set("sphere_scale", 30)
cmd.ray(2400, 2400)
cmd.png(save_file_30, dpi=300)

cmd.set("sphere_scale", 60)
cmd.ray(2400, 2400)
cmd.png(save_file_60, dpi=300)

cmd.set("sphere_scale", 100)
cmd.ray(2400, 2400)
cmd.png(save_file_100, dpi=300)

cmd.set("sphere_scale", 500)
cmd.ray(2400, 2400)
cmd.png(save_file_500, dpi=300)
