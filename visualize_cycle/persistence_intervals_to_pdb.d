import std.stdio, std.conv, std.string, std.array, std.algorithm;


version(unittest){}
 else{
   void main(string[] args)
   {
     auto intervalFile = args[1];
     auto targetLine = args[2].to!int;
     auto outFile = args[3];

     auto coords = readCoord(intervalFile, targetLine);
     auto pdb = coordsToPdb(coords);

     auto fout = File(outFile, "w");
     foreach(entry; pdb)
       fout.writeln(entry);
   }
 }


double[][] readCoord(string filename, int targetLine)
{
  double[][] coords;
  int i = 0;
  
  foreach(line; File(filename, "r").byLine) {
    if(i == targetLine) {
      auto fields = line.to!string.strip.split("\t");
      foreach(point; fields[3..$])
        coords ~= point.split(',').map!(x => x.to!double).array;
      break;
    }
    i++;
  }
  return coords;
}


string[] coordsToPdb(double[][] coords)
{
  string[] entries;
  string[] connectEntries;
  auto chrIDs = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9",
                 "A", "B", "C", "D", "E", "F", "G", "H", "I", "J",
                 "K", "L", "M", "N", "O", "p", "Q", "R", "S", "T",
                 "U", "v", "W", "X", "Y", "Z",];
  auto chrID = 0;
  auto coordID = 0;
  
  foreach(i, coord; coords) {
      // 1..6 (6)
      auto atom = "HETATM";
      // 7..11 (5)
      auto serial = format("%5d", i);
      if(serial.length != 5) throw new Exception("ERROR: serial.length != 5");
      // 13..16 (1 + 4)
      auto name = "     ";
      // 17 (1)
      auto altLoc = " ";
      // 18..20 (3)
      auto resName = "   ";
      // 22 (1 + 1)
      auto chainID = " " ~ chrIDs[chrID];
      if(chainID.length != 2) throw new Exception("ERROR: chainID.length != 2");
      // 23..26 (4)
      auto resSeq = "    ";
      // 27 (1)
      auto iCode = " ";
      // 31..38 (3 + 8)
      auto x = format("   %8.3f", coords[coordID][0]);
      if(x.length != 11) throw new Exception("ERROR: x.length != 11");
      // 39..46 (8)
      auto y = format("%8.3f", coords[coordID][1]);
      if(y.length != 8) throw new Exception("ERROR: y.length != 8");
      // 47..54 (8)
      auto z = format("%8.3f", coords[coordID][2]);
      if(z.length != 8) throw new Exception("ERROR: z.length != 8");
      // 55..60 (6)
      auto occupancy = "      ";
      // 61..66 (6)
      auto tempFactor = "      ";
      // 77..78 (10 + 2)
      auto element = "            ";
      // 79..80 (2)
      auto charge = "  ";

      entries ~= atom ~ serial ~ name ~ altLoc ~ resName ~ chainID ~ resSeq
        ~ iCode ~ x ~ y ~ z ~ occupancy ~ tempFactor ~ element ~ charge;

      coordID++;
  }


  auto ter = "TER   ";
  auto serial = "     ";
  auto resName = "   ";
  auto chainID = " " ~ chrIDs[chrID];
  if(chainID.length != 2)
    throw new Exception("ERROR: chainID.length != 2");             
  auto resSeq = "    ";
  auto iCode = " ";
      
  entries ~= ter ~ serial ~ resName ~ chainID ~ resSeq ~ iCode;
  
  foreach(i, noUse; coords) {
    for(auto j = i + 1; j < coords.length; j++) {
      auto id1 = format("%5d", i);
      auto id2 = format("%5d", j);
      if(id1.length != 5 || id2.length != 5)
        throw new Exception("ERROR: CONECT length != 5");
      
      entries ~= "CONECT" ~ id1 ~ id2 ~ "               ";
      
    }
  }
        
  entries ~= "END   ";
  
  return entries;
}
