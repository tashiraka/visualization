zoom = 1000


def setup():
    size(500, 500, P3D)
        
        
def draw():
    background(200) #black background
    translate(width/2, height/2) #set the origin on the center of display
    camera(zoom, 0, 0, 0, 0, 0, 0, 1, 0)
    rotateY(radians(mouseX))
    rotateZ(radians(mouseY))
    
    convex_hull_file = 'convex_hull'
    point_on_convex_hull_file = 'point_on_convex_hull'
    
    for line in open(convex_hull_file, 'r'):
        fields = line.rstrip().split('\t')
        p1 = map(float, fields[0].split(','))
        p2 = map(float, fields[1].split(','))
        p3 = map(float, fields[2].split(','))

        fill(255, 150)
        beginShape(TRIANGLE)
        vertex(p1[0], p1[1], p1[2])
        vertex(p2[0], p2[1], p2[2])
        vertex(p3[0], p3[1], p3[2])
        endShape()

    for line in open(point_on_convex_hull_file, 'r'):
        p = map(float, line.rstrip().split('\t'))
        fill(255,0,0)
        strokeWeight(10)
        beginShape(POINTS)
        vertex(p[0], p[1], p[2])
        endShape()

def mouseWheel(event):
    global zoom 
    zoom += event.getCount() * 20

